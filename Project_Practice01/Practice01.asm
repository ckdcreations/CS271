TITLE Dog Years (Project_Practice01.asm)

; Author:
; Course / Project ID                 Date:
; Description:

INCLUDE Irvine32.inc

MAXSIZE = 4

.data
array	DWORD	MAXSIZE DUP(?)
.code
main PROC

push	MAXSIZE
push	8
push	OFFSET array
call	whatzit

retAdd:
; ...

mov		eax, OFFSET array
call	WriteDec
call	CrLf
	exit	; exit to operating system

main ENDP

whatzit PROC

push	ebp
mov		ebp, esp
mov		edi, [ebp+8]
mov		eax, 0
mov		ebx, [ebp+12]
mov		ecx, [ebp+16]
mov		edx, 0

fill:

add		eax, ebx
mov		[edi+edx], eax
inc		ebx
dec		eax
add		edx, 4
loop	fill

pop		ebp
ret		12

whatzit ENDP
; (insert additional procedures here)

END main