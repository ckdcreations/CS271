TITLE Assignment 6     (Project06.asm)

; Name: Brandon Lee
; Email: leebran@onid.oregonstate.edu
; Class: CS271 Section 400
; Assignment: #6A
; Due Date: 6/7/15

INCLUDE Irvine32.inc
quiz4   MACRO p,q
        LOCAL here
        push   eax
        push   ecx
        mov    eax, p
        mov    ecx, q
here:
        mul    P
        loop   here

        mov    p, eax
		push	p
		pop	ebx
        pop    ecx
        pop    eax
ENDM
.data
x       DWORD 3
y       DWORD 3

.code

main proc

 mov    ebx, 3
   mov    ecx, 12
   mov    edx, ecx
   quiz4  ecx, ebx

mov	eax, edx
call writedec
call crlf

	exit		; exit to operating system
main ENDP




end main
