TITLE Programming Assignment #5 (Program05.asm)

; Author: Clifford Dunn - dunncli@oregonstate.edu
; Course / Project ID  CS271 Computer Architecture & Assembly Language               Due Date: November 20, 2016
; Description: Program #5: Random Numbers Generating and Sorting 

INCLUDE Irvine32.inc

; Constant Definitions

	MIN = 10
	MAX = 200

	LO = 100
	HI = 999

	MAX_NAME_LENGTH = 24

.data

; Textual Outputs Through the Program
	intro_01			BYTE	"This is the Random Number Generator/Sorter",0
	intro_02			BYTE	"Programmed by Clifford Dunn",0

	greet_01			BYTE	"Hello! What's your name? ",0
	greet_02			BYTE	"Hello, ",0

	prompt_01			BYTE	"How many random numbers would you like to generate?",0
	prompt_02			BYTE	"I'll accept a value between 10 and 200.",0
	prompt_03			BYTE	"Enter the number of random numbers to generate [10 .. 200]: ",0

	error_01			BYTE	"Out of range. Please try again.",0

	list_title_01		BYTE	"The unsorted random numbers:",0

	median_string		BYTE	"The median is: ",0
	period				BYTE	".",0

	list_title_02		BYTE	"The sorted list:",0

	goodbye_01			BYTE	"Results certified by Clifford Dunn",0
	goodbye_02			BYTE	"Goodbye, ",0

	five_spaces			BYTE	"     ",0
; Max Name Length is necessary to create a buffer for entering a name
	user_Name			BYTE	MAX_NAME_LENGTH DUP(?)

	num_rand_num		DWORD	?	; How many random numbers should be generated?
	rand_num_array		DWORD	MAX	DUP(?)	; Create memory for array with 200 values



;-------------------------------------------------------------------------------------------------------------------
;-------------------------------------------------------------------------------------------------------------------
; Executing code
;-------------------------------------------------------------------------------------------------------------------
;-------------------------------------------------------------------------------------------------------------------
.code

;-------------------------------------------------------------------------------------------------------------------
; Procedure Name: introduction. 
; Description: This procedure outputs this title of the program and the author
; Input: None.
; Output: Outputs the title and programmer name
;-------------------------------------------------------------------------------------------------------------------
introduction PROC
	push				ebp
	mov					ebp, esp
	mov					edx, [ebp+12]					; intro_01: Title
	call				WriteString
	call				CrLf
	mov					edx, [ebp+8]					; intro_02: Programmer Name
	call				WriteString
	call				CrLf
	call				CrLf
	pop					ebp
	ret					8
introduction ENDP


;-------------------------------------------------------------------------------------------------------------------
; Procedure Name: getUserName
; Description: This procedure receives the user's name and greets them.
; Input: User must enter a name.
; Output: Standard greeting to the user
;-------------------------------------------------------------------------------------------------------------------
getUserName PROC
	push				ebp
	mov					ebp, esp
	mov					edx, [ebp+16]				; greet_01: "Hello! What's your name?"
	call				WriteString
	call				CrLf
	mov					edx, [ebp+8]				; user_name
	mov					ecx, MAX_NAME_LENGTH
	call				ReadString
	call				CrLf
	mov					edx, [ebp+12]				; greet_02: "Hello, [user_name]"
	call				WriteString
	mov					edx, [ebp+8]				; Outputs user_name
	call				WriteString
	call				CrLf
	pop					ebp
	ret					12
getUserName ENDP



;-------------------------------------------------------------------------------------------------------------------
; Procedure Name: getUserData
; Description: This procedure asks for the desired amount of composite numbers.
; Input: User must enter a number in the range of [10 .. 200].
; Output: Displays a prompt asking for amount of numbers. Validates this to the proper range.
;-------------------------------------------------------------------------------------------------------------------
getData PROC
	push				ebp
	mov					ebp, esp
	prompt:
	mov					edx, [ebp+12]				; prompt_01: Display the initial instruction for the user
	call				WriteString
	call				CrLf
	mov					edx, [ebp+16]				; prompt_02: Further instruction
	call				WriteString
	call				CrLf
	mov					edx, [ebp+20]				; prompt_03: Further instruction
	call				WriteString
	mov					ebx, [ebp+8]				; num_rand_num
	call				ReadInt
	cmp					eax, MIN
	jl					invalid
	cmp					eax, MAX
	jg					invalid
	jmp					valid

	invalid:
	call				CrLf
	mov					edx, [ebp+24]			; error_01: Out of range
	call				WriteString
	call				CrLf
	call				CrLf
	jmp					prompt

	valid:
	mov					[ebx], eax
	pop					ebp
	ret					20
getData ENDP



;-------------------------------------------------------------------------------------------------------------------
; Procedure Name: fillArray 
; Description: This procedure generates as many random numbers as necessary to fill the array.
; Input: From main, the address of the array and the value of the number of random numbers
; Output: No visual output. This will simply generate all the necessary values ad put them into the array.
;-------------------------------------------------------------------------------------------------------------------
fillArray PROC
	push				ebp
	mov					ebp, esp
	mov					edi, [ebp+12]				; Move the address of the array to the edi register
	mov					ecx, [ebp+8]				; Move the value of num_rand_num to the looping register.

	fillingArray:
	; Code borrowed from lecture video: CS271 Lecture # 20
	mov					eax, HI						; Place the HI value in eax register
	sub					eax, LO						; Subtract the LO value from the HI
	inc					eax							; Add 1 to the value
	call				RandomRange
	add					eax, LO						; Re-add the LO value so random number sits within the 
													; proper range.

	mov					[edi], eax					; Place the value in the first available spot of the array.
	add					edi, 4						; Move to the next available spot in the array.
	loop				fillingArray

	pop					ebp
	ret					8
fillArray ENDP



;-------------------------------------------------------------------------------------------------------------------
; procedure name: displaylist 
; description: Prints the list
; input: Array passed by reference. Array size passed by value. Title of list.
; output: Visual output of the array in the order it exists.
;-------------------------------------------------------------------------------------------------------------------
displayList PROC
	push				ebp
	mov					ebp, esp
	mov					esi, [ebp+20]					; Address of the array
	mov					ecx, [ebp+16]					; Value of the array's size placed into the counter register

	call				CrLf
	mov					edx, [ebp+12]					; list_title
	call				WriteString
	call				CrLf
	mov					ebx, 0							; Create accumulator for determining spaces or lines

	printing:
	inc					ebx								; Accumulate ebx register
	mov					eax, [esi]						; Move the value from the current array index to eax register.
	call				WriteDec						; Print the value
	add					esi, 4							; Move forward 4 bytes to the next index of the array
	cmp					ebx, 10							; Compare the ebx register to 10
	jne					addSpace						; If ebx != 10, jump to addSpace
	call				CrLf							; Otherwise, create new line
	mov					ebx, 0							; Reset ebx to 0
	jmp					contPrint						; Jump to continue printing

	addSpace:
	mov					edx, [ebp+8]					; create five_spaces
	call				WriteString

	contPrint:
	loop				printing						; Loop the printing of the array
	call				CrLf

	pop					ebp
	ret					16

displayList ENDP

;-------------------------------------------------------------------------------------------------------------------
; procedure name: sortList
; description: Implementation of a bubble sort. Recursive sort would be ideal, but bubble sort was simpler.
; input: Address for array. Value of num_rand_num
; output: Bubble sorts the array into descending order
;-------------------------------------------------------------------------------------------------------------------
sortList PROC

	LOCAL				num_rand:DWORD					; Attempted to implement with only stack references. 
														; Complications arose because of the nested loop.
	
	mov					esi, [ebp+12]					; Address for array
	mov					eax, [ebp+8]					; num_rand_num
	mov					num_rand, eax					; Assign the count to local variable

	dec					num_rand						; offset for 0 value
	mov					ecx, num_rand					; Place the variable into the loop counter

	; outLoop: loop through each position in the array.
	outLoop:
	push				ecx								; Push the loop value onto the stack. Saves the count for outLoop.
	mov					ecx, num_rand					; Assign local variable to the loop counter for inner loop.
	mov					esi, [ebp+12]					; Reset to the beginning of the array

	; nestLoop: Look at each value in the array and compare it to the next value.
	; If the next value is greater, swap the values. Otherwise, move to the next value.
	nestLoop:
	mov					eax, [esi]						; Move the first value into eax register
	cmp					[esi+4], eax					; Compare the next value to the first
	jl					lesser							; If second value is less, jump to lesser
	push				[esi+4]							; Otherwise, push the first value onto stack.
	push				[esi]							; Then push the second value onto stack
	call				swap							; Call swap procedure.
	pop					[esi]							; Pop the value off the stack.
	pop					[esi+4]							; Pop the next value off the stack.

	; lesser: If the next value is already less than the current value, move to 
	; next value and continue loop.
	lesser:
	add					esi, 4							; Move to next position in the array
	loop				nestLoop						; Loop through the inner loop.
	
	pop 				ecx								; Pop the stack value (original ecx) back into the ecx register
	loop 				outLoop							; Loop the outLoop

	ret					8

sortList ENDP


;-------------------------------------------------------------------------------------------------------------------
; procedure name: swap
; description: Swaps two values.
; input: Address for two indexes in the array. 
; output: Swaps the values in their memory locations
;-------------------------------------------------------------------------------------------------------------------
swap PROC
	push	ebp
	mov		ebp, esp

	mov		eax, [ebp+12]					; Place the low value in eax register
	mov		ebx, [ebp+8]					; Place the hi value in ebx register
	mov		[ebp+12], ebx					; Place the value in ebx register (hi) to the position where low value is.
	mov		[ebp+8], eax					; Place the value in eax register (lo) to the position where hi value is. 
	pop		ebp
	ret										; Do not clean up stack because it is still in use
swap ENDP


;-------------------------------------------------------------------------------------------------------------------
; procedure name: displayMedian
; description: Find the median value of the array.
; input: Address of array. Value of counter. Median and period strings
; output: The median value is displayed on a separate line
;-------------------------------------------------------------------------------------------------------------------
displayMedian PROC
	push				ebp
	mov					ebp, esp
	mov					edi, [ebp+20]					; Address of array

	call				CrLf
	mov					edx, [ebp+12]					; median_string
	call				WriteString

	cdq													; Convert double to quad
	mov					eax, [ebp+16]					; Counter
	mov					ebx, 2							; Place value 2 in ebx register
	div					ebx								; Divide counter in half
	mov					ebx, eax						; Place result in ebx register
	cmp					edx, 0							; Compare result to 0
	jz					evenNum							; If zero, that means number is even

	; Else it is odd
	mov					eax, [edi+eax*4]				; Place the middle value in eax register
	call				WriteDec						; Print it
	jmp					periodSpot						; Jump to print period

	; Jumped to if the number was even
	evenNum:
	mov					ecx, [edi+eax*4]				; Place the first median number in ecx register
	dec					eax								; Decrement eax to find the second median number
	mov					ebx, [edi+eax*4]				; Place the second median number in ebx register
	mov					eax, ecx						; Place first median number back in eax register for division
	add					eax, ebx						; Add the two median numbers
	cdq													; Convert double to quad
	mov					ebx, 2							; Place the value 2 in ebx register
	div					ebx								; Divide eax by 2
	cmp					edx, 0							; Check if number divided evenly
	jz					printing						; If it did, jump to printing
	inc					eax								; Otherwise round up the number
	printing:
	call				WriteDec

	periodSpot:
	mov					edx, [ebp+8]					; period
	call				WriteString
	call				CrLf
	
	pop					ebp
	ret					16

displayMedian ENDP



;--------------------------------------------------------------------------------------------------------------
; Procedure Name: farewell
; Description: This procedure issues a farewell to the user.
; Input: None.
; Output: Standard farewell to the user.
;--------------------------------------------------------------------------------------------------------------
farewell PROC
	push				ebp
	mov					ebp, esp

	call				CrLf
	mov					edx, [ebp+16]					; goodbye_01
	call				WriteString
	call				CrLf
	call				CrLf
	mov					edx, [ebp+12]					; goodbye_02
	call				WriteString
	mov					edx, [ebp+8]					; user_name
	call				WriteString
	call				CrLf

	pop					ebp
	ret					12
farewell ENDP






;--------------------------------------------------------------------------------------------------------------
; Procedure Name: main
; Description: This is the main procedure.
; Input: None.
; Output: A script type procedure that calls the other procedures as necessary..
;--------------------------------------------------------------------------------------------------------------
main PROC

	; Call to randomization. This is necessary so every instance of the program being run will have new 
	; random values.

	call				Randomize

	; Introduction. Displays the title and programmer's name.

	push				OFFSET intro_01
	push				OFFSET intro_02
	call				introduction		

	; Prompts for the user's name.	
	push				OFFSET greet_01
	push			    OFFSET greet_02
	push				OFFSET user_Name		
	call				getUserName

	; Push the memory for num_rand_num onto the stack. Call getData to figure out how much memory to use.
	
	push				OFFSET error_01
	push				OFFSET prompt_03
	push				OFFSET prompt_02
	push				OFFSET prompt_01
	push				OFFSET num_rand_num		; Pass by reference the variable to know how many numbers
												; to generate. This will be filled in getData procedure.
	call				getData

	; Push the array onto the stack. Push value of num_rand_num onto stack.
	; Call fillArray in order to fill the array with values.

	push				OFFSET rand_num_array	; Pass by reference to the array
	push				num_rand_num			; Pass the value of num_rand_num
	call				fillArray				
	
	; Push the array onto the stack. Push the value of the count onto the stack. 
	; Push title for unsorted array onto the stack. Call displayList procedure

	push				OFFSET rand_num_array
	push				num_rand_num
	push				OFFSET list_title_01
	push				OFFSET five_spaces
	call				displayList

	; Push the array onto the stack. Push the value of the count onto the stack.
	; Call the sortList procedure to sort the list into descending order.
	push				OFFSET rand_num_array
	push				num_rand_num
	call				sortList


	; Push the array onto the stack. Push the value of the count onto the stack.
	; Push the median_string and period variables onto the stack. 
	; Call the procedure to output and calculate the median of the numbers
	push				OFFSET rand_num_array
	push				num_rand_num
	push				OFFSET median_string
	push				OFFSET period
	call				displayMedian

	; Push the array onto the stack. Push the value of the count onto the stack. 
	; Push title for sorted array onto the stack. Call displayList procedure

	push				OFFSET rand_num_array
	push				num_rand_num
	push				OFFSET list_title_02
	push				OFFSET five_spaces
	call				displayList


	; Bids farewell to the user before exiting the program.	
	push				OFFSET goodbye_01
	push				OFFSET goodbye_02
	push				OFFSET user_name
	call				farewell

	exit	; exit to operating system
main ENDP


END main