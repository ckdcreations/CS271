TITLE Assignment 4     (Project04.asm)

; Name: Brandon Lee
; Email: leebran@onid.oregonstate.edu
; Class: CS271 Section 400
; Assignment: #4
; Due Date: 4/10/15

; Description: The fourth homework assignment - procedures, loops, nested loops, data validation

INCLUDE Irvine32.inc

	LOWER = 1
	UPPER = 400

.data
	introMsg		BYTE		"TITLE: Assignment 4 - Brandon Lee",0
	byeMsg			BYTE		"Goodbye",0
	instruct0		BYTE		"Enter the number of composite numbers you would like to see.",0
	instruct1		BYTE		"Please enter numbers in [1 .. 400]: ",0
	errMsg			BYTE		"Your input is not valid",0
	spaces			BYTE		"   ",0

	userInput		DWORD		?
	compositeNum	DWORD		?
	lineCounter		DWORD		0

.code
main PROC
   push 2
   push 15
   call rcrsn
   exit
main ENDP

rcrsn PROC
   push ebp
   mov ebp,esp
   mov eax,[ebp + 12]
   mov ebx,[ebp + 8]
   cmp eax,ebx
   jl recurse
   jmp quit
recurse:
   inc eax
   push eax
   push ebx
   call rcrsn
   mov eax,[ebp + 12]
   call WriteDec
   mov edx, OFFSET spaces
   call WriteString 
quit:
   pop ebp
   ret 8
rcrsn ENDP



END main
