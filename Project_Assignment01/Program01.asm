TITLE Programming Assignment #1 (Program01.asm)

; Author: Clifford Dunn
; Course / Project ID  CS271 Computer Architecture & Assembly Language               Date: September 26, 2016
; Description: Automatic Arithmetic in Assembly!

INCLUDE Irvine32.inc

; (insert constant definitions here)

.data

num_01			DWORD	?	; First Prompted Number
num_02			DWORD	?	; Second Prompted Number
sum				DWORD	?	; Variable to store the sum
diff			DWORD	?	; Variable to store the difference
prod			DWORD	?	; Variable to store the product
quot			DWORD	?	; Variable to store the quotient
remain			DWORD	?	; Variable to store the quotient remainder
	
intro_01		BYTE	"Automatic Arithmetic in Assembly!	By Clifford Dunn", 0
intro_02		BYTE	"Give me two numbers, and I will display the sum, difference, product, quotient, and remainder.", 0

prompt_01		BYTE	"First number: ", 0
prompt_02		BYTE	"Second number: ", 0

; These variables will be used in between the numbers for visual output of the mathematical operations.
sumOut			BYTE	" + ", 0
diffOut			BYTE	" - ", 0
prodOut			BYTE	" x ", 0
quotOut			BYTE	" / ", 0
equals			BYTE	" = ", 0
remainOut		BYTE	" remainder ", 0

; Extra credit variables
ec_01			BYTE	"*** Extra Credit: This program will loop until the user ends it. ***",0
ec_02			BYTE	"*** Extra Credit: This program ensures that the second number is less than the first. ***",0

ec_01Question	BYTE	"Would you like to go again? Input 1 for yes or any other key for no. ",0
ec_01User		DWORD	?

ec_02Warning		BYTE	"The second number must be less than the first.",0

; Goodbye Variable
goodbye			BYTE	"Thank you for playing! Goodbye!",0


.code
main PROC

; Introduction
	mov			edx, OFFSET intro_01
	call		WriteString				; Display the first introduction message.
	call		CrLf
	call		CrLf
	mov			edx, OFFSET ec_01
	call		WriteString				; Display the first extra credit message.
	call		CrLf
	mov			edx, OFFSET ec_02
	call		WriteString				; Display the second extra credit message.
	call		CrLf
	call		CrLf

; Prompt Part 1
	mov			edx, OFFSET intro_02
	call		WriteString
	call		CrLf
	call		CrLf

; numInputs is the section where the user must enter numbers.
numInputs:
; Prompt for First Number
	mov			edx, OFFSET prompt_01
	call		WriteString
	call		readInt
	mov			num_01, eax

; Prompt for Second Number
	mov			edx, OFFSET prompt_02
	call		WriteString
	call		readInt
	mov			num_02, eax
	call		CrLf

; Extra Credit: Compare the numbers and jump back if second number is larger
	mov			eax, num_02
	cmp			eax, num_01
	jg			inError
	jle			calculation

; inError is called when the second number is greater than the first number.
inError:
	mov			edx, OFFSET ec_02Warning
	call		WriteString
	call		CrLf
	jg			numInputs		; If inError has been called, the warning is written and then a jump to numInputs is called.	
	

; calculation is called assuming the second number is less than the first.
calculation:
; Add the numbers together
    mov			eax, num_01
	add			eax, num_02
	mov			sum, eax

; Subtract the numbers	
	mov			eax, num_01
	sub			eax, num_02
	mov			diff, eax

; Multiply the numbers
	mov			eax, num_01
	mov			ebx, num_02
	mul			ebx 
	mov			prod, eax

; Divide the numbers 
	mov		edx, 0
	mov		eax, num_01
	mov		ebx, num_02 
	div		ebx
	mov		quot, eax
	mov		remain, edx

; Sum results
	mov			eax, num_01
	call		WriteDec
	mov			edx, OFFSET sumOut
	call		WriteString
	mov			eax, num_02
	call		WriteDec
	mov			edx, OFFSET equals 
	call		WriteString
	mov			eax, sum
	call		WriteDec
	call		CrLf

; Difference results
	mov			eax, num_01
	call		WriteDec
	mov			edx, OFFSET diffOut
	call		WriteString
	mov			eax, num_02
	call		WriteDec
	mov			edx, OFFSET equals 
	call		WriteString
	mov			eax, diff
	call		WriteDec
	call		CrLf	

; Product results
	mov			eax, num_01
	call		WriteDec
	mov			edx, OFFSET prodOut
	call		WriteString
	mov			eax, num_02
	call		WriteDec
	mov			edx, OFFSET equals
	call		WriteString
	mov			eax, prod
	call		WriteDec
	call		CrLf

; Quotient results
	mov			eax, num_01
	call		WriteDec
	mov			edx, OFFSET quotOut
	call		WriteString
	mov			eax, num_02
	call		WriteDec
	mov			edx, OFFSET equals
	call		WriteString
	mov			eax, quot
	call		WriteDec
	;call		CrLf

; Remainder results
	mov			edx, OFFSET remainOut 
	call		WriteString
	mov			eax, remain
	call		WriteDec
	call		CrLf

; Prompt the user for another round
	mov			edx, OFFSET ec_01Question
	call		WriteString
	call		ReadInt
	mov			ec_01User, eax
	cmp			eax, 1
	je			numInputs			

; Goodbye
	mov			edx, OFFSET goodbye
	call		WriteString
	call		CrLf

	exit	; exit to operating system
main ENDP

; (insert additional procedures here)

END main