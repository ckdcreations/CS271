TITLE Programming Assignment #4 (Program04.asm)

; Author: Clifford Dunn
; Course / Project ID  CS271 Computer Architecture & Assembly Language               Date: November 2, 2016
; Description: Program #4: Composite Numbers 

INCLUDE Irvine32.inc

; Constant Definitions
	LOWER_LIMIT	= 1
	UPPER_LIMIT = 400

	MAX_NAME_LENGTH = 24

.data

; Textual Outputs Through the Program
	intro_01			BYTE	"This is the Composite Number Generator",0
	intro_02			BYTE	"Programmed by Clifford Dunn",0
	extra_credit		BYTE	"EC: Output columns are aligned!",0

	greet_01			BYTE	"Hello! What's your name? ",0
	greet_02			BYTE	"Hello, ",0

	prompt_01			BYTE	"How many composite numbers would you like to see?",0
	prompt_02			BYTE	"I'll accept orders for up to 400 composite numbers.",0
	prompt_03			BYTE	"Enter the number of composites to display [1 .. 400]: ",0

	error_01			BYTE	"Out of range. Please try again.",0

	goodbye_01			BYTE	"Results certified by Clifford Dunn",0
	goodbye_02			BYTE	"Goodbye, ",0

	five_spaces			BYTE	"     ",0
	four_spaces			BYTE	"    ",0
	three_spaces		BYTE	"   ",0
; Max Name Length is necessary to create a buffer for entering a name
	user_Name			BYTE	MAX_NAME_LENGTH DUP(?)

	num_composite		DWORD	?	; How many composite numbers should be displayed?
	next_composite		DWORD	?	; The composite number to display next
	num_outs			DWORD	0	; Number of output lines



;-------------------------------------------------------------------------------------------------------------------
;-------------------------------------------------------------------------------------------------------------------
; Executing code
;-------------------------------------------------------------------------------------------------------------------
;-------------------------------------------------------------------------------------------------------------------
.code

;-------------------------------------------------------------------------------------------------------------------
; Procedure Name: introduction. 
; Description: This procedure outputs this title of the program and the author
; Input: None.
; Output: Outputs the title and programmer name
;-------------------------------------------------------------------------------------------------------------------
introduction PROC

	mov					edx, OFFSET intro_01
	call				WriteString				
	call				CrLf
	mov					edx, OFFSET intro_02
	call				WriteString
	call				CrLf
	mov					edx, OFFSET extra_credit
	call				WriteString
	call				CrLf
	call				CrLf
	ret

introduction ENDP


;-------------------------------------------------------------------------------------------------------------------
; Procedure Name: getUserName
; Description: This procedure receives the user's name and greets them.
; Input: User must enter a name.
; Output: Standard greeting to the user
;-------------------------------------------------------------------------------------------------------------------
getUserName PROC
	mov					edx, OFFSET greet_01
	call				WriteString
	mov					edx, OFFSET user_Name		; Create a space for the user name
	mov					ecx, MAX_NAME_LENGTH		; Enter the user name
	call				ReadString
	call				CrLf
	mov					edx, OFFSET greet_02
	call				WriteString
	mov					edx, OFFSET user_Name
	call				WriteString
	call				CrLf
	ret
getUserName ENDP



;-------------------------------------------------------------------------------------------------------------------
; Procedure Name: getUserData
; Description: This procedure asks for the desired amount of composite numbers.
; Input: User must enter a number in the range of [1 .. 400].
; Output: Displays a prompt asking for numbers. Calls the validation procedure to make sure num_composite is valid.
;-------------------------------------------------------------------------------------------------------------------
getUserData PROC
	mov					edx, OFFSET prompt_01		; Display the initial instruction for the user
	call				WriteString
	call				CrLf
	mov					edx, OFFSET prompt_02		; Further instruction
	call				WriteString
	call				CrLf
	mov					edx, OFFSET prompt_03		; Further instruction
	call				WriteString
	call				ReadInt
	mov					num_composite, eax
	call				CrLf
	call				validation
	ret
getUserData ENDP



;-------------------------------------------------------------------------------------------------------------------
; Procedure Name: spaceOrJump
; Description: This procedure determines the proper spacing and changes the output.
; Input: Looks at both the num_outs variable and next_composite variable.
; Output: Changes the number of spaces between numbers depending on their size or adds a new line.
;-------------------------------------------------------------------------------------------------------------------
spaceOrJump PROC
	inc					num_outs					; Accumulator for starting new lines when necessary
	mov					eax, num_outs
	cmp					eax, 10						; When the accumulator reaches ten, it will jump to addLine.
	je					addLine

	; Jump to addFiveSpaces if the number is single digit
	cmp					next_composite, 10
	jb					addFiveSpaces
	; Jump to addFourSpaces if the number is double digit
	cmp					next_composite, 100
	jb					addFourSpaces
	; Jump to addThreeSpaces if the number is double digit
	cmp					next_composite, 1000
	jb					addThreeSpaces

; Adds five spaces after each number
addFiveSpaces:
	inc					next_composite				; Increase the next_composite variable in order to test for new composite
	mov					edx, OFFSET five_spaces
	call				WriteString
	ret

; Adds four spaces after each number
addFourSpaces:
	inc					next_composite				; Increase the next_composite variable in order to test for new composite
	mov					edx, OFFSET four_spaces
	call				WriteString
	ret

; Adds three spaces after each number
addThreeSpaces:
	inc					next_composite				; Increase the next_composite variable in order to test for new composite
	mov					edx, OFFSET three_spaces
	call				WriteString
	ret

; Create a new line
addLine:
	inc					next_composite				; Increase the next_composite variable in order to test for new composite
	mov					eax, 0
	mov					num_outs, eax				; Resets the accumulator to 0 for future line counting
	call				CrLf
	ret
spaceOrJump ENDP



;-------------------------------------------------------------------------------------------------------------------
; Procedure Name: showComposites 
; Description: This procedure outputs the number of composite numbers desired.
; Input: None.
; Output: List of composite numbers
;-------------------------------------------------------------------------------------------------------------------
showComposites PROC
	mov					ecx, num_composite			; Moves the desired number of composites into the loop counter
	mov					eax, 4						; Sets the first register to 4 which is the first composite
	mov					next_composite, eax			; Sets the next_composite variable to 4
	mov					ebx, 2						; Used to perform division on potential composites

mainLoop:
	call				isComposite

	; Once the next composite is found, display it.
	mov					eax, next_composite
	call				WriteDec
	
	; Determine the proper spacing for the output.	
	call				spaceOrJump

	; Proceed on through the loop
	mov					eax, next_composite			; Place the next_composite variable in the proper register
	mov					ebx, 2						; Prepare the ebx register for comparison to next_composite
	loop				mainLoop					; Go back to the beginning of the loop process
showComposites ENDP



;-------------------------------------------------------------------------------------------------------------------
; Procedure Name: isComposite	 
; Description: Determines if the following number is composite.
; Input: None.
; Output: List of composite numbers
;-------------------------------------------------------------------------------------------------------------------
isComposite PROC
compositeTest:
	cmp					eax, ebx					; Check if the next_composite and the potential divisor are the same
	je					noComposite					; If they are, jump ahead to noComposite

	cdq												; Convert double to quad. Makes the division work
	div					ebx							
	cmp					edx, 0						; Check if the values divide evenly and there is no remainder
	je					posComposite				; If the values divide, the number is composite
	jne					negComposite				; If they don't, jump ahead
	
posComposite:
	ret												; The number is composite and the procedure will end

negComposite:
	mov					eax, next_composite			 
	inc					ebx							; Increases the value of the number to divide into next_composite
	jmp					compositeTest				; Start the process over to find a composite number

; The original next_composite value turned out to be a prime number
noComposite:
	inc					next_composite				; Increase the value of next_composite				
	mov					ebx, 2						; Restart the increasing divisors
	jmp					compositeTest				; Jump back to the beginning of the composite test
isComposite ENDP



;-------------------------------------------------------------------------------------------------------------------
; Procedure Name: validation 
; Description: This procedure checks the num_composite variable and make sure it falls within the desired range.
; Input: Looks at the number that was initially entered.
; Output: Either continues with the program or reprompts the user if the number is out of range.
;-------------------------------------------------------------------------------------------------------------------
validation PROC
	; Determines if the entered number is lower than 1 or greater than 400.
	cmp					eax, LOWER_LIMIT
	jl					outOfRange
	cmp					eax, UPPER_LIMIT
	jg					outOfRange
	ret

; Procedure jumps here if the desired number of composites is out of range
outOfRange:
	mov					edx, OFFSET error_01
	call				WriteString
	call				CrLf
	jmp					getUserData		; Sends the user back to reenter their desired number of composites.
validation ENDP



;--------------------------------------------------------------------------------------------------------------
; Procedure Name: farewell
; Description: This procedure issues a farewell to the user.
; Input: None.
; Output: Standard farewell to the user.
;--------------------------------------------------------------------------------------------------------------
farewell PROC
	call				CrLf
	mov					edx, OFFSET goodbye_01
	call				WriteString
	call				CrLf
	call				CrLf
	mov					edx, OFFSET goodbye_02
	call				WriteString
	mov					edx, OFFSET user_Name
	call				WriteString
	call				CrLf
	ret
farewell ENDP






;--------------------------------------------------------------------------------------------------------------
; Procedure Name: main
; Description: This is the main procedure.
; Input: None.
; Output: A script type procedure that calls the other procedures as necessary..
;--------------------------------------------------------------------------------------------------------------
main PROC

	; Introduction. Displays the title and programmer's name.
	call				introduction		

	; Prompts for the user's name.	
	call				getUserName

	; Asks the user how many composite numbers to display.	
	call				getUserData

	; Performes the calculations and displays the composite numbers
	call				showComposites

	; Bids farewell to the user before exiting the program.	
	call				farewell

	exit	; exit to operating system
main ENDP


END main