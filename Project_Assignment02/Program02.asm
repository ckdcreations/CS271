TITLE Programming Assignment #2 (Program02.asm)

; Author: Clifford Dunn
; Course / Project ID  CS271 Computer Architecture & Assembly Language               Date: October 15, 2016
; Description: Program #2: Write a program to calculate Fibonacci numbers.

INCLUDE Irvine32.inc

; Constant Definitions
	LOWER_LIMIT	= 1
	UPPER_LIMIT = 46

	MAX_NAME_LENGTH = 24

.data

; Textual Outputs Through the Program
	intro_01			BYTE	"Fibonacci Fun!",0
	intro_02			BYTE	"Programmed by Clifford Dunn",0

	greet_01			BYTE	"Hello! What's your name? ",0
	greet_02			BYTE	"Hello, ",0

	prompt_01			BYTE	", please enter the number of Fibonacci terms to be displayed.",0
	prompt_02			BYTE	"Please give the number as an integer in the range of 1-46.",0
	prompt_03			BYTE	"How many Fibonacci numbers do you want? ",0

	error_01			BYTE	"Out of range.",0
	error_02			BYTE	"Selected value is too low.",0
	error_03			BYTE	"Selected value is too high.",0

	goodbye_01			BYTE	"Results certified by Clifford Dunn",0
	goodbye_02			BYTE	"Goodbye, ",0

; Max Name Length is necessary to create a buffer for entering a name
	user_Name			BYTE	MAX_NAME_LENGTH DUP(?)
	num_Fibs			DWORD	? ; How many Fibonacci numbers do we want?

	fib_01				DWORD	0 ; Initial value
	fib_02				DWORD	1 ; First Value

	nextFib				DWORD	? ; Place Holder for next Fibonacci number

	fiveSpace			BYTE	"     ",0 ; Create five spaces after each value

.code
main PROC

introduction:

; Initial Title 
	mov					edx, OFFSET intro_01
	call				WriteString
	call				CrLf
	mov					edx, OFFSET intro_02
	call				WriteString
	call				CrLf
	call				CrLf



userInstructions:

; Greeting the User
	mov					edx, OFFSET greet_01
	call				WriteString
	mov					edx, OFFSET user_Name		; Create a space for the user name
	mov					ecx, MAX_NAME_LENGTH		; Enter the user name
	call				ReadString
	call				CrLf
	mov					edx, OFFSET greet_02
	call				WriteString
	mov					edx, OFFSET user_Name
	call				WriteString
	call				CrLf

getUserData:
	mov					edx, OFFSET user_Name		; Display the user name
	call				WriteString
	mov					edx, OFFSET prompt_01		; Display the initial instruction for the user
	call				WriteString
	call				CrLf
	mov					edx, OFFSET prompt_02		; Further instruction
	call				WriteString
	call				CrLf
	mov					edx, OFFSET prompt_03		; Ask for user input
	call				WriteString
	call				ReadInt
	mov					num_Fibs, eax				; Read the input and put the value in eax
	call				CrLf

; Input validation
	cmp					eax, LOWER_LIMIT			; Make sure the value entered isn't lower than 1
	jb					tooLow				
	cmp					eax, UPPER_LIMIT			; Make sure the value entered isn't higher than 46
	jg					tooHigh
	mov					ecx, num_Fibs
	mov					eax, 1
	call				WriteDec
	jmp					addSpaces	

; If user enters a number lower than 1
tooLow:
	mov					edx, OFFSET error_01
	call				WriteString
	call				CrLf
	mov					edx, OFFSET error_02
	call				WriteString
	call				CrLf
	call				CrLf
	jmp					getUserData				; Goes back to the prompts for more user data

; If user enters a number higher than 46
tooHigh:
	mov					edx, OFFSET error_01
	call				WriteString
	call				CrLf
	mov					edx, OFFSET error_03
	call				WriteString
	call				CrLf
	call				CrLf
	jmp					getUserData				; Goes back to the prompts for more user data

; Outputs the Fibonacci numbers
displayFibs:
	mov					eax, fib_01
	mov					ebx, fib_02
	mov					fib_01, ebx
	add					eax, ebx				; Adds the previous number to the most recent number
	mov					fib_02, eax
	call				WriteDec

; Countdown to create a new line or to add spaces after each number
	mov					edx, 0
	mov					ebx, 5
	mov					edx, ecx
	cdq										; Converts double to quad. Necessary for division and remainder
	div					ebx
	cmp					edx, 0
	je					addLine
	jne					addSpaces	

; Adds five spaces after each number
addSpaces:
	mov					edx, OFFSET fiveSpace
	call				WriteString
	jmp					callLoop

; Adds a line after five numbers
addLine:
	call				CrLf
	jmp					callLoop

; Goes back to the loop to display the Fibonacci sequence
callLoop:
	loop				displayFibs

; Bid farewell to the user and end
farewell:
	call				CrLf
	mov					edx, OFFSET goodbye_01
	call				WriteString
	call				CrLf
	call				CrLf
	mov					edx, OFFSET goodbye_02
	call				WriteString
	mov					edx, OFFSET user_Name
	call				WriteString
	call				CrLf



	exit	; exit to operating system
main ENDP


END main