TITLE Programming Assignment #6 (Program06.asm)

; Author: Clifford Dunn - dunncli@oregonstate.edu
; Course / Project ID  CS271 Computer Architecture & Assembly Language               Due Date: December 4, 2016
; Description: Program #6: Low Level I/O Procedures 

INCLUDE Irvine32.inc

; Constant Definitions

	ARRAY_SIZE = 10
	MAX_NAME_LENGTH = 24

;--------------------------------------------------------------------------------------------------------------
; Macro	Name: stackFrame
; Description: This is the standard  
; Input: None.
; Output: Basic procedure to set up a procedure stack
;--------------------------------------------------------------------------------------------------------------
stackFrame MACRO
	
	push				ebp
	mov					ebp, esp

ENDM



;--------------------------------------------------------------------------------------------------------------
; Macro Name: getString 
; Description: This macro reads an incoming string.
; Input: Address to read, length of the address.
; Output: String is stored.
;--------------------------------------------------------------------------------------------------------------
getString			MACRO addr, length
	
	push				edx
	push				ecx
	mov					edx, addr
	mov					ecx, length
	call				ReadString
	pop					ecx
	pop					edx

ENDM



;--------------------------------------------------------------------------------------------------------------
; Macro Name: displayString
; Description: This macro displays a string.
; Input: a String.
; Output: Displays the string.
;--------------------------------------------------------------------------------------------------------------
displayString		MACRO string

	push				edx
	mov					edx, string
	call				WriteString
	pop					edx

ENDM


.data

; Textual Outputs Through the Program
	intro_01			BYTE	"This Program Demonstrates Low Level I/O Procedures",0
	intro_02			BYTE	"Programmed by Clifford Dunn",0

	extra_credit		BYTE	"**EC: Each valid line is numbered and a running sum is displayed.",0

	greet_01			BYTE	"Hello! What's your name? ",0
	greet_02			BYTE	"Hello, ",0

	prompt_01			BYTE	"Please provide 10 unsigned integers.",0
	prompt_02			BYTE	"Each number must fit inside of a 32 bit register.",0
	prompt_03			BYTE	"Enter an unsigned integer: ",0

	error_01			BYTE	"Input is invalid. Please try again. ",0

	result_01			BYTE	"The numbers you entered are: ",0
	result_02			BYTE	"The sum of all the numbers is: ",0
	result_03			BYTE	"The average of all the numbers is: ",0


	goodbye_01			BYTE	"Results certified by Clifford Dunn",0
	goodbye_02			BYTE	"Goodbye, ",0

	period_spaces		BYTE	".     ",0

	temp_sum			BYTE	"The current sum is: ",0
	
	input_buf			BYTE	255 DUP(0)
	string_buf			BYTE	32 DUP(0)

	sum					DWORD	?
	avg					DWORD	0
	num_array			DWORD	10 DUP(0)

; Max Name Length is necessary to create a buffer for entering a name
	user_Name			BYTE	MAX_NAME_LENGTH DUP(?)

;-------------------------------------------------------------------------------------------------------------------
;-------------------------------------------------------------------------------------------------------------------
; Executing code
;-------------------------------------------------------------------------------------------------------------------
;-------------------------------------------------------------------------------------------------------------------
.code

;-------------------------------------------------------------------------------------------------------------------
; Procedure Name: introduction. 
; Description: This procedure outputs this title of the program and the author
; Input: None.
; Output: Outputs the title and programmer name
;-------------------------------------------------------------------------------------------------------------------
introduction PROC
	; Notes: ebp+12 = intro_01; ebp+8 = intro_02
	STACKFRAME

	displayString		[ebp+12]						; intro_01: Title
	call				CrLf
	displayString		[ebp+8]							; intro_02: Programmer Name
	call				CrLf
	displayString		[ebp+16]
	call				CrLf
	call				CrLf
	pop					ebp
	ret					12
introduction ENDP


;-------------------------------------------------------------------------------------------------------------------
; Procedure Name: getUserName
; Description: This procedure receives the user's name and greets them.
; Input: User must enter a name.
; Output: Standard greeting to the user
;-------------------------------------------------------------------------------------------------------------------
getUserName PROC
	; NOTES: ebp+20 = greet_01; ebp+16 = user_Name; ebp+12 = MAX_NAME_LENGTH; ebp+8 = greet_02
	STACKFRAME

	displayString		[ebp+20]
	call				CrLf
	getString			[ebp+16], [ebp+12]
	call				CrLf
	displayString		[ebp+8]
	displayString		[ebp+16]
	call				CrLf
	pop					ebp
	ret					16
getUserName ENDP




;-------------------------------------------------------------------------------------------------------------------
; Procedure Name: inputLoop 
; Description: This procedure takes 10 unsigned integer inputs and stores them in an array.
; Input: Variable for temp_sum, multiple string outputs, input and string buffers, array to fill.
; Output: A complete array will be filled and returned
;-------------------------------------------------------------------------------------------------------------------
inputLoop PROC
	; NOTES: ebp+48 = temp_sum;
	; NOTES: ebp+44 = period_spaces; ebp+40 = prompt_01; ebp+36 = prompt_02; ebp+32 = prompt_03; ebp+28 = error_01;
	; NOTES: ebp+24 = input_buf; ebp+20 = sizeof input_buf; ebp+16 = string_buf; ebp+12 = num_array; ebp+8 = sum
	STACKFRAME
	pushad

	mov					ecx, 10								; Set the loop counter
	mov					ebx, 1								; Set the accumulator (EC)
	mov					edi, [ebp+12]						; Place the array into the edi register
	displayString		[ebp+40]							; Print first prompt
	call				CrLf
	displayString		[ebp+36]							; Print second prompt
	call				CrLf

gather:
	push				ebx									; Push the accumulator for display
	push				[ebp+16]							; Push the buffer
	call				WriteVal							; Display the value of the accumulator
	displayString		[ebp+44]							; Period plus spaces
	displayString		[ebp+48]							; Introduce the current sum
	push				[ebp+8]								; Push the value of the current sum
	push				[ebp+16]							; Push the buffer
	call				WriteVal							; Display the current Sum
	displayString		[ebp+44]							; Period plus spaces
	displayString		[ebp+32]							; Prompt to enter an integer
	push				[ebp+28]							; Push the error message
	push				[ebp+24]							; Push the input buffer
	push				[ebp+20]							; Push the string buffer
	call				ReadVal								; Call procedure to gather the value

	mov					eax, DWORD PTR input_buf			; Take the value aat the current buffer
	add					[ebp+8], eax						; Add that value to the current sum
	mov					[edi], eax							; Add that value to the temp array
	add					edi, 4								; Move to the next value in the temp array
	inc					ebx									; Increase the accumulator
	loop				gather


	popad
	pop					ebp
	ret					44
inputLoop ENDP




;-------------------------------------------------------------------------------------------------------------------
; Procedure Name: arrayOut 
; Description: This procedure displays each value of the array.
; Input: 1 string output, the array, size of the array, and a string buffer.
; Output: The array will be displayed with each value on a separate line.
;-------------------------------------------------------------------------------------------------------------------
arrayOut PROC
	; NOTES: ebp+20 = ARRAY_SIZE; ebp+16 = num_array; ebp+12 = result_01; ebp+8 = string_buf
	STACKFRAME
	pushad

	mov					ecx, [ebp+20]								; Set the loop counter
	mov					esi, [ebp+16]								; The array
	
	displayString		[ebp+12]									; Result string out
	call				CrLf

displayArray:
	mov					eax, [esi]									; Take the current value and place in eax
	push				eax											; Push current value on the stack
	push				[ebp+8]										; Push the buffer
	call				WriteVal									; Call procedure to display the value
	call				CrLf
	add					esi, 4										; Move to the next value in array
	loop				displayArray	
	popad
	pop					ebp
	ret					16

arrayOut ENDP

;-------------------------------------------------------------------------------------------------------------------
; Procedure Name: sumAvgOut 
; Description: This procedure displays the sum and average of all entered integers.
; Input: 2 string outputs, the array, size of the array, string buffer, placeholders for average and sum.
; Output: The output will show the sum and average of all integers.
;-------------------------------------------------------------------------------------------------------------------
sumAvgOut PROC
	; NOTES: ebp+32 = ARRAY_SIZE; ebp+28 = num_array; ebp+24 = result_02; 
	; NOTES: ebp+20 = result_03; ebp+16 = sum; ebp+12 = average; ebp+8 = string_buf
	STACKFRAME
	pushad

	mov					ecx, [ebp+32]									; Set loop counter
	mov					esi, [ebp+28]									; The array
	mov					ebx, 0											; Start the sum
addthem:
	mov					eax, [esi]										; Take the current value
	add					ebx, eax										; Add it to the ebx register
	add					esi, 4											; Move to the next value
	loop				addThem

	mov					[ebp+16], ebx									; Place the sum in sum

	call				CrLf
	call				CrLf
	call				CrLf

	displayString		[ebp+24]										; The sum is:
	;mov					[ebp+16], ebx
	push				[ebp+16]										; Push the sum 
	push				[ebp+16]										; Push the sum again
	push				[ebp+8]											; Push the buffer
	call				WriteVal										; Display the sum
	call				CrLf
	call				CrLf
	call				CrLf

	pop					eax												; Pop the sum into eax
	mov					ebx, 10											; Prepare to calculate average
	cdq																	; Convert double to quad
	div					ebx												; Calculate the average (rounded down)

	
	displayString		[ebp+20]										; The average is
	push				eax												; Push the value of the average
	push				[ebp+8]											; Push the buffer
	call				WriteVal										; Display the average
	call				CrLf
	call				CrLf
	call				CrLf

	popad
	pop					ebp
	ret					28

	

	sumAvgOut ENDP

;-------------------------------------------------------------------------------------------------------------------
; Procedure Name: ReadVal. 
; Description: This procedure reads the integer value that is input
; Input: Buffer for the input as well as the size of the buffer.
; Output: Returns the value that is entered.
;-------------------------------------------------------------------------------------------------------------------
ReadVal		PROC

	STACKFRAME
	pushad
start:

	mov					edx, [ebp+12]				; The address of the buffer
	mov					ecx, [ebp+8]				; The size of the buffer

	getString			edx, ecx					; Get the value and store it in edx

	mov					esi, edx					; lodsb reads from the esi register, so the value must be stored there.
	mov					eax, 0						; Make sure registers are clear			
	mov					ecx, 0						; Make sure registers are clear
	mov					ebx, 10						; For conversion b 

loadByte:
	lodsb											; Load the value of the esi register
	cmp					ax, 0						; Check to see if the current value is the end of the string
	je					done						; If it is, jump to done

	; ASCII values determined at www.asciitable.com
	cmp					ax, 48						; Check to make sure the value is at or above 0 (ASCII Value)
	jb					error						; If it is lower, jump to error
	cmp					ax, 57						; Make sure the value is at or below 9 (ASCII Value)
	ja					error						; If it is higher, jump to error

	sub					ax, 48						; Convert ASCII to Decimal Value
	xchg				eax, ecx					; Swap eax and ecx
	mul					ebx							; Multiply by 10 in order to get proper value
	jc					error						; If value is too large, jump to error
	jnc					noError						; Otherwise jump to noError


error:
	displayString		[ebp+16]					; Display error message.
	jmp					start						; start over

noError:
	add					eax, ecx					; Add the digit to the register
	xchg				eax, ecx					; swap registers
	jmp					loadByte					; Loop the search

done:
	xchg				ecx, eax					; Final swap to ensure the right value is in the right register
	mov					DWORD PTR input_buf, eax	; Place the value in the current input's pointed value

	popad
	pop					ebp
	ret					12

ReadVal		ENDP

;-------------------------------------------------------------------------------------------------------------------
; Procedure Name: WriteVal. 
; Description: This procedure outputs the value to the screen. 
; Input: Value and string buffer.
; Output: Displays the value.
;-------------------------------------------------------------------------------------------------------------------
WriteVal PROC
	STACKFRAME
	pushad

	mov					eax, [ebp+12]									; The integer to display
	mov					edi, [ebp+8]									; Buffer address to store
	mov					ebx, 10											; For division
	push				0												; To indicate buffer end

convert:
	mov					edx, 0											; Clear the register
	div					ebx												; Convert integer
	add					edx, 48											; Convert remainder to proper format
	push				edx												; push the next digit onto stack

	cmp					eax, 0											; Check if the string is finished
	jne					convert											; If not, repeat the conversion

popVal:
	pop					[edi]											; Remove the address of the previous digit
	mov					eax, [edi]										; Move the current value to eax
	inc					edi												; Increment to the next buffer value
	cmp					eax, 0											; Check to see if the buffer is empty
	jne					popVal											; If not, repeat

	
	mov					edx, [ebp+8]									; move to edx register for macro
	displayString		[ebp+8]											; Display the value	
	
	popad
	pop					ebp
	ret					8

WriteVal ENDP





;--------------------------------------------------------------------------------------------------------------
; Procedure Name: farewell
; Description: This procedure issues a farewell to the user.
; Input: None.
; Output: Standard farewell to the user.
;--------------------------------------------------------------------------------------------------------------
farewell PROC
	; Notes: ebp+16 = goodbye_01; ebp+12 = goodbye_02; ebp+8 = user_Name;
	STACKFRAME
	call				CrLf
	displayString		[ebp+16]
	call				CrLf
	call				CrLf
	displayString		[ebp+12]
	displayString		[ebp+8]
	call				CrLf

	pop					ebp
	ret					12
farewell ENDP 

;--------------------------------------------------------------------------------------------------------------
; Procedure Name: main
; Description: This is the main procedure.
; Input: None.
; Output: A script type procedure that calls the other procedures as necessary..
;--------------------------------------------------------------------------------------------------------------
main PROC


	; Introduction. Displays the title and programmer's name.
	push				OFFSET extra_credit
	push				OFFSET intro_01
	push				OFFSET intro_02
	call				introduction



	; Prompts for the user's name.	
	push				OFFSET greet_01
	push				OFFSET user_Name
	push				MAX_NAME_LENGTH
	push				OFFSET greet_02
	call				getUserName

	

	; Instruction. Asks the user for 10 unsigned integers
	push				OFFSET temp_sum
	push				OFFSET period_spaces
	push				OFFSET prompt_01
	push				OFFSET prompt_02
	push				OFFSET prompt_03
	push				OFFSET error_01
	push				OFFSET input_buf
	push				SIZEOF input_buf
	push				OFFSET string_buf
	push				OFFSET num_array
	push				sum
	call				inputLoop

	; Display the array
	push				ARRAY_SIZE
	push				OFFSET num_array
	push				OFFSET result_01
	push				OFFSET string_buf
	call				arrayOut



	; Display the sum and the average
	push				ARRAY_SIZE
	push				OFFSET num_array
	push				OFFSET result_02
	push				OFFSET result_03
	push				OFFSET sum
	push				avg
	push				OFFSET string_buf
	call				sumAvgOut



	; Bids farewell to the user before exiting the program.	
	push				OFFSET goodbye_01
	push				OFFSET goodbye_02
	push				OFFSET user_name
	call				farewell


	exit	; exit to operating system


main ENDP


END main