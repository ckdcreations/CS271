TITLE Programming Assignment #3 (Program03.asm)

; Author: Clifford Dunn
; Course / Project ID  CS271 Computer Architecture & Assembly Language               Date: October 26, 2016
; Description: Program #3: Integer Accumulation

INCLUDE Irvine32.inc

; Constant Definitions
	LOWER_LIMIT	= -100
	UPPER_LIMIT = -1

	MAX_NAME_LENGTH = 24

.data

; Textual Outputs Through the Program
	intro_01			BYTE	"This is the auto integer accumulator",0
	intro_02			BYTE	"Programmed by Clifford Dunn",0

	greet_01			BYTE	"Hello! What's your name? ",0
	greet_02			BYTE	"Hello, ",0

	prompt_01			BYTE	"Please enter numbers within the range of -100 and -1.",0
	prompt_02			BYTE	"When finished, input a non-negative number to see the results.",0
	prompt_03			BYTE	"Enter number ",0
	prompt_04			BYTE	": ",0

	result_01			BYTE	"You entered ",0
	result_02			BYTE	" valid numbers.",0
	result_03			BYTE	"The sum of your valid numbers is: ",0
	result_04			BYTE	"The rounded average of your valid numbers is: ",0

	error_01			BYTE	"Out of range.",0
	error_02			BYTE	"Selected value is too low. Please pick another.",0

	ending_01			BYTE	"You have selected an ending number.",0
	

	empty_goodbye_01	BYTE	"There were no valid inputs. Try again sometime!",0

	goodbye_01			BYTE	"Results certified by Clifford Dunn",0
	goodbye_02			BYTE	"Goodbye, ",0

; Max Name Length is necessary to create a buffer for entering a name
	user_Name			BYTE	MAX_NAME_LENGTH DUP(?)

	total_Inputs		DWORD	0 ; Keeps track of how many numbers have been input.
	input_Sum			DWORD	0 ; The sum of all valid numbers.
	input_Avg			DWORD	? ; The average of all valid numbers.
	new_Input			DWORD	? ; New number input
	
	
.code
main PROC

introduction:

; Initial Title
; This displays the title, and introduction of the programmer
	mov					edx, OFFSET intro_01
	call				WriteString				
	call				CrLf
	mov					edx, OFFSET intro_02
	call				WriteString
	call				CrLf
	call				CrLf



userInstructions:

; Greeting the User
	mov					edx, OFFSET greet_01
	call				WriteString
	mov					edx, OFFSET user_Name		; Create a space for the user name
	mov					ecx, MAX_NAME_LENGTH		; Enter the user name
	call				ReadString
	call				CrLf
	mov					edx, OFFSET greet_02
	call				WriteString
	mov					edx, OFFSET user_Name
	call				WriteString
	call				CrLf

; Beginning of the process for data input. getUserData section will display verbose instructions for the user.
; The mainLoop will continue gathering data from the user. 
getUserData:
	mov					edx, OFFSET prompt_01		; Display the initial instruction for the user
	call				WriteString
	call				CrLf
	mov					edx, OFFSET prompt_02		; Further instruction
	call				WriteString
	call				CrLf

; Data gathering
mainLoop:

; Input validation
	mov					edx, OFFSET prompt_03
	call				WriteString
	mov					eax, total_Inputs			; Prepares the total_Inputs variable for display
	add					eax, 1						; Increase the value of total_Inputs for display purposes only
	
	; EC: NUMBER THE LINES DURING USER INPUT
	; This occurs in the middle of the output prompt so it looks like a more natural and logical sentence. 
	call				WriteDec
	mov					edx, OFFSET prompt_04
	call				WriteString
	call				ReadInt
	mov					new_Input, eax				; Reads the user input and places it in the new_Input variable

	; Input validation to ensure the numbers are in the proper range
	cmp					eax, LOWER_LIMIT			; Make sure the value entered isn't lower than -100
	jl					tooLow						; If it is too low, jump to tooLow section	
	cmp					eax, UPPER_LIMIT			; Make sure the value entered isn't higher than -1
	jg					tooHigh						; If it is too high, jump to tooHigh section

	; Perform the addition operation
	mov					eax, input_Sum				; Prepare the input_Sum variable
	add					eax, new_Input				; Add the new_Input variable to the value that is in input_Sum
	mov					input_Sum, eax				; Overwrite the input_Sum variable with the new sum that was added.

	;mov					ebx, total_Inputs
	inc					total_Inputs				; Increments the actual value of total_Inputs

	
	loop				mainLoop					; go back to mainLoop and repeat

; If user enters a number lower than -100
tooLow:
	mov					edx, OFFSET error_01
	call				WriteString
	call				CrLf
	mov					edx, OFFSET error_02
	call				WriteString
	call				CrLf
	call				CrLf
	jmp					getUserData				; Goes back to the prompts for more user data with the verbose intro

; If user enters a number higher than -1
tooHigh:
	mov					edx, OFFSET ending_01
	call				WriteString
	call				CrLf
	call				CrLf
	cmp					total_Inputs, 1
	jl					farewell2			; If there have been no valid inputs, jump to farewell2
	jmp					farewell			; Otherwise, simply go to the farewell section

; Bid farewell to the user and end
farewell:
	mov					edx, OFFSET result_01
	call				WriteString

	; Display the total number of valid inputs
	mov					eax, total_Inputs
	call				WriteDec
	mov					edx, OFFSET result_02
	call				WriteString
	call				CrLf

	; Display the total sum of valid inputs
	mov					edx, OFFSET result_03
	call				WriteString
	mov					eax, input_Sum
	call				WriteInt
	call				CrLf

	; Perform the division operation necessary for average output
	mov					edx, 0
	mov					eax, input_Sum
	cdq
	mov					ebx, total_Inputs
	idiv				ebx
	mov					input_Avg, eax

	; Display the rounded integer average
	mov					edx, OFFSET result_04
	call				WriteString
	mov					eax, input_avg
	call				WriteInt
	call				CrLf

	; Display verification and goodbye	
	mov					edx, OFFSET goodbye_01
	call				WriteString
	call				CrLf
	call				CrLf
	mov					edx, OFFSET goodbye_02
	call				WriteString
	mov					edx, OFFSET user_Name
	call				WriteString
	call				CrLf
	jmp					quit						; Jumps to absolute end

; Special case ending when no valid entries were input
farewell2:
	mov					edx, OFFSET empty_Goodbye_01
	call				WriteString
	call				CrLf
	call				CrLf
	mov					edx, OFFSET goodbye_02
	call				WriteString
	mov					edx, OFFSET user_Name
	call				WriteString
	call				CrLf
	jmp					quit


quit:


	exit	; exit to operating system
main ENDP


END main